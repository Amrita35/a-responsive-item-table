// info is an array of objects//
// Each object has three attributes, iname: Item name, ides: Item description, and ingrediants

var info=[
{iname: 'Hawaiian Pizza Supreme', ides:'ini 8" 6.99,)   (Mini 8" Thin/Crispy 6.99)   (Mini 8" Thick 7.99)   (Small 10" 12.99)   (Small 10" Thin/Crispy 12.99)   (Small 10" Thick 13.99)   (Medium 12" 14.99)   (Medium 12" Thin/Crispy 14.99)   (Medium 12" Thick 15.99)   (Large 14"  16.99)   (Large 14" Thin/Crispy 16.99)   (Large 14" Thick 17.99)   (XLarge 19.99)   (XLarge Thin/Crispy 19.99)   (XLarge Thick 22.99)', ingrediants: "Canadian Bacon and Pineapple with Extra Mozzarella Cheese"},
{iname: 'Combination Pizza Supreme', ides:'Mini 8" 6.99)   (Mini 8" Thin/Crispy 6.99)   (Mini 8" Thick 7.99)   (Small 10" 12.99)   (Small 10" Thin/Crispy 12.99)   (Small 10" Thick 13.99)   (Medium 12" 14.99)   (Medium 12" Thin/Crispy 14.99) ', ingrediants: 'White Onions, Green Peppers, Pepperoni, Ham, Sausage, Mushrooms, Olives and Extra Mozzarella Cheese'},
{iname: 'Meat Eaters Pizza Supreme', ides:'Mini 8" 6.99)   (Mini 8" Thin/Crispy 6.99)   (Mini 8" Thick 7.99)   (Small 10" 12.99)   (Small 10" Thin/Crispy 12.99)   (Small 10" Thick 13.99)   (Medium 12" 14.99)   (Medium 12" Thin/Crispy 14.99)   (Medium 12" Thick 15.99)   (Large 14"  16.99)   (Large 14" Thin/Crispy 16.99)   (Large 14" Thick 17.99)   (XLarge 19.99)   (XLarge Thin/Crispy 19.99)  ', ingrediants: 'Pepperoni, Ham, Sausage, Beef and Extra Mozzarella Cheese'},
{iname: 'Garden Veggie Pizza Supreme', ides:'Mini 8" 6.99)   (Mini 8" Thin/Crispy 6.99)   (Mini 8" Thick 7.99)   (Small 10" 12.99)   (Small 10" Thin/Crispy 12.99)   (Small 10" Thick 13.99)   (Medium 12" 14.99)   (Medium 12" Thin/Crispy 14.99)   (Medium 12" Thick 15.99)  (Large 14" Thick 17.99)   (XLarge 19.99)   (XLarge Thin/Crispy 19.99)  ', ingrediants: 'White Onions, Green Peppers, Mushrooms, Olives, Tomatoes and Extra Mozzarella Cheese'},
{iname: 'Hawaiian Pizza Supreme', ides:'ini 8" 6.99,)   (Mini 8" Thin/Crispy 6.99)   (Mini 8" Thick 7.99)   (Small 10" 12.99)   (Small 10" Thin/Crispy 12.99)   (Small 10" Thick 13.99)   (Medium 12" 14.99)   (Medium 12" Thin/Crispy 14.99)   (Medium 12" Thick 15.99)   (Large 14"  16.99)   (Large 14" Thin/Crispy 16.99)   (Large 14" Thick 17.99)   (XLarge 19.99)   (XLarge Thin/Crispy 19.99)   (XLarge Thick 22.99)', ingrediants: "Canadian Bacon and Pineapple with Extra Mozzarella Cheese"},
{iname: 'Combination Pizza Supreme', ides:'Mini 8" 6.99)   (Mini 8" Thin/Crispy 6.99)   (Mini 8" Thick 7.99)   (Small 10" 12.99)   (Small 10" Thin/Crispy 12.99)   (Small 10" Thick 13.99)   (Medium 12" 14.99)   (Medium 12" Thin/Crispy 14.99) ', ingrediants: 'White Onions, Green Peppers, Pepperoni, Ham, Sausage, Mushrooms, Olives and Extra Mozzarella Cheese'},
{iname: 'Meat Eaters Pizza Supreme', ides:'Mini 8" 6.99)   (Mini 8" Thin/Crispy 6.99)   (Mini 8" Thick 7.99)   (Small 10" 12.99)   (Small 10" Thin/Crispy 12.99)   (Small 10" Thick 13.99)   (Medium 12" 14.99)   (Medium 12" Thin/Crispy 14.99)   (Medium 12" Thick 15.99)   (Large 14"  16.99)   (Large 14" Thin/Crispy 16.99)   (Large 14" Thick 17.99)   (XLarge 19.99)   (XLarge Thin/Crispy 19.99)  ', ingrediants: 'Pepperoni, Ham, Sausage, Beef and Extra Mozzarella Cheese'},
{iname: 'Garden Veggie Pizza Supreme', ides:'Mini 8" 6.99)   (Mini 8" Thin/Crispy 6.99)   (Mini 8" Thick 7.99)   (Small 10" 12.99)   (Small 10" Thin/Crispy 12.99)   (Small 10" Thick 13.99)   (Medium 12" 14.99)   (Medium 12" Thin/Crispy 14.99)   (Medium 12" Thick 15.99)  (Large 14" Thick 17.99)   (XLarge 19.99)   (XLarge Thin/Crispy 19.99)  ', ingrediants: 'White Onions, Green Peppers, Mushrooms, Olives, Tomatoes and Extra Mozzarella Cheese'}
]

//for loop to create table and loop through the array of objects
var text="";
for (i = 0; i < info.length; i++) {
  text+="<tr><td><h1 onclick=AddItem()>"+info[i].iname+"</h1><h2>"+info[i].ides+"</h2><p>"+info[i].ingrediants+"</td> <td class=buttonClass><button onclick=AddItem()>Add item</button></td></tr>";
}

document.getElementById("itable").innerHTML=text;

//This function gets called whenever the ADD item button and item name gets clicked//
function AddItem() {
   alert("Clicked");
}
